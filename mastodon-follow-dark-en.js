/* SPDX-License-Identifier: Zlib
   Copyright (C) 2022 smpl <smpl@slamkode.ml> */

var mastodon_follow_settings = {
	"account": "superman@fortitude.fortress",
	"portal": "https://fortitude.fortress/users/superman/remote_follow",
	"dialog": { 
		"width": "256px",
		"background": "#282c37",
		"color": "#9baec8",
		"title": "#fff",
		"input": {
			"bordercolor": "#0a0b0e",
			"background": "#131419",
			"color": "#fff",
			"placeholder": "#c83737"
		},
		"button": {
			"background": "#2b90d9",
			"color": "#fff"
		}
	}
};
var mastodon_follow_strings = 
{
	"title": "Enter your Mastodon handle",
	"button": "Follow",
	"create": "Create account",
	"create_url": "https://joinmastodon.org/communities",
	"error": { "handle": "Invalid handle" }
};

var mastodon_follow_container = null;

/* Remove nojs fallback link, when JS enabled. */
window.addEventListener("load", function(e) {
	mastodonfollow.style.cursor = "pointer";
	var a = mastodonfollow.parentNode;
	var c = a.parentNode;
	c.appendChild(mastodonfollow);
	a.remove();
});

function mastodon_follow_create_style()
{
	var style = document.createElement('style');
	var d = mastodon_follow_settings.dialog;
	style.innerHTML =
		".mastodon-follow-container {" +
			"display: block;" + 
			"z-index: 1;" + 
			"border: 0;" + 
			"border-radius: 5px;" +
			"background: " + d.background + ";" + 
			"position: absolute;" + 
			"top:110%;" + 
			"left: 0;" +
			"width: " + d.width + ";" +
			'font-family: "Roboto",Roboto,sans-serif;' +
			"margin: 0 auto;" +
			"padding: 20px;" +
			"font-weight: 400;" +
			"font-size: 13px;" +
			"color: " + d.color + ";" +
			"line-height: 18px;" +
		"}" +
		".mastodon-follow-title {" +
			"text-align: center;" + 
			"font-size: 18px;" +
			"font-weight: 500;" + 
			"margin: 0;" +
			"color: " + d.title + ";" +
		"}" +
		".mastodon-follow-input {" +
			"border: 0;" +
			"border-radius: 4px;" +
			"margin-top: 20px;" +
			"padding: 10px;" +	
			"background: " + d.input.background + ";" +
			"width: 100%;" +
			"box-sizing: border-box;" +
			"font-family: inherit;" +
			"font-weight: 500;" +
			"font-size: 18px;" +
			"color: " + d.input.color + ";" +
		"}" +
		".mastodon-follow-input::placeholder {" +
			"color: " + d.input.placeholder + ";" +
		"}" +
		".mastodon-follow-button {" +
			"display: block;" +
			"width: 100%;" +
			"border: 0;" +
			"border-radius: 4px;" +
			"margin-right: 10px;" +
			"margin-top: 20px;" +
			"padding: 10px;" +
			"background: " + d.button.background + ";" +
			"font-family: inherit;" +
			"font-weight: 500;" +
			"font-size: 18px;" +
			"color: " + d.button.color + ";" +
			"line-height: inherit;" +
			"text-align: center;" +
			"text-transform: uppercase;" +
			"box-sizing: border-box;" +
			"cursor: pointer;" +
		"}";
		document.head.appendChild(style);
}

/* Called when request to instance is complete.
 * Parse the JSON response and find the subscribe uri.
 * On error goes to the instances remote_follow url. */
function mastodon_follow_go(event)
{
	var data;
	
	if (this.readyState === this.DONE) {
		if (this.status === 200) {
			try {
				data = JSON.parse(this.responseText);
			} catch {
				window.location = mastodon_follow_settings.portal;
				return;
			}
			
			data.links.forEach( function(e) {
				if(e.rel === "http://ostatus.org/schema/1.0/subscribe") {
					window.location = e.template.replace('{uri}', encodeURIComponent(mastodon_follow_settings.account));
					return;
				}
			});
		}
	}
	window.location = mastodon_follow_settings.portal;
	return;
}

/* Called when user click on follow button in dialog.
 * Grab the mastodon handle from the input field and request the 
 * appropriate webfinger document from the users instance.
 * Response is handled by mastodon_follow_go(). */
function mastodon_follow_lookup(event)
{
	var handle_element = document.querySelector('input[name="mastodon-handle"]');
	var handle = handle_element.value;
	
	// remove any initial @
	if(handle.charAt(0) == '@') {
		handle = handle.substring(1);
	}
	
	// split into nick and host
	var uriparts = handle.split('@');
	if(uriparts.length < 2) {
		handle_element.value = '';
		handle_element.setAttribute('placeholder', mastodon_follow_strings.error.handle);
		return;
	}

	var oReq = new XMLHttpRequest();
	oReq.addEventListener("load", mastodon_follow_go);
	oReq.open("GET", "https://" + uriparts[1] + "/.well-known/webfinger?resource=acct:" + encodeURIComponent(handle));
	oReq.send();
}

/* Activated when icon is clicked.
 * Create the follow dialog on first run, toggle visibility on further
 * clicks. */
function mastodon_follow_show()
{
	if(!mastodon_follow_container) {
		mastodon_follow_create_style();
		
		mastodon_follow_container = document.createElement('div');
		mastodon_follow_container.setAttribute('class', 'mastodon-follow-container');
		
		var mastodon_title = document.createElement('p');
		mastodon_title.setAttribute('class', 'mastodon-follow-title');
		mastodon_title.innerHTML = mastodon_follow_strings.title;
		
		var mastodon_follow_handle = document.createElement('input');
		mastodon_follow_handle.setAttribute('name', 'mastodon-handle');
		mastodon_follow_handle.setAttribute('type', 'text');
		mastodon_follow_handle.setAttribute('class', 'mastodon-follow-input');
		
		var mastodon_follow_follow = document.createElement('button');
		mastodon_follow_follow.innerHTML = mastodon_follow_strings.button;
		mastodon_follow_follow.setAttribute('class', 'mastodon-follow-button');
		mastodon_follow_follow.addEventListener('click', mastodon_follow_lookup);
		
		var createaccount = document.createElement('div');
		createaccount.style = "text-align: right;";
		var createaccount_a = document.createElement('a');
		createaccount_a.setAttribute('href', mastodon_follow_strings.create_url);
		createaccount_a.innerHTML = mastodon_follow_strings.create;
		createaccount.appendChild(createaccount_a);
		
		mastodon_follow_container.appendChild(mastodon_title);
		mastodon_follow_container.appendChild(mastodon_follow_handle);
		mastodon_follow_container.appendChild(mastodon_follow_follow);
		mastodon_follow_container.appendChild(createaccount);
		
		mastodonfollow.parentElement.appendChild(mastodon_follow_container);
	} else {
		mastodon_follow_container.style.display = 
			mastodon_follow_container.style.display == "none" ? "block" : "none";
	}
}
